def are_lists_equal(list1, list2):
    '''
    this functin gets 2 list and checks if they are the same
    :param list1:
    :param list2:
    :return:
    '''
    return (sorted(list(list1)) == sorted(list(list2)))